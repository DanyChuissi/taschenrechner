package com.taschenrechner.taschenrechnerbackend.controller;

import com.taschenrechner.taschenrechnerbackend.model.OperationModel;
import com.taschenrechner.taschenrechnerbackend.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * the operation controller
 * @author Dany Chuissi
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/operation")
public class OperationController {
    /**
     * the Operation service
     */
    @Autowired
    OperationService service;

    /**
     * get all Operation
     *
     * @return a {@link ResponseEntity}
     */
    @GetMapping
    public ResponseEntity<List<OperationModel>> getAllOperation() {
        List<OperationModel> list = service.getAllOperations();

        return new ResponseEntity<>(list, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * create operation
     *
     * @param operation the operation model
     * @return a {@link ResponseEntity}
     */
    @PostMapping
    public ResponseEntity<OperationModel> createOperation(@RequestBody OperationModel operation) {
        OperationModel updated = service.createOperations(operation);
        return new ResponseEntity<>(updated, new HttpHeaders(), HttpStatus.OK);
    }
}
