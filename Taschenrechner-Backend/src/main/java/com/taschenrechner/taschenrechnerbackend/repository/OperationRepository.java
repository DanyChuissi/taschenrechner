package com.taschenrechner.taschenrechnerbackend.repository;

import com.taschenrechner.taschenrechnerbackend.model.OperationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Operation repository.
 * the manage the {@link com.taschenrechner.taschenrechnerbackend.model.OperationModel} database object
 * @author Dany Chuissi
 */
@Repository
public interface OperationRepository
        extends JpaRepository<OperationModel, Long> {

}