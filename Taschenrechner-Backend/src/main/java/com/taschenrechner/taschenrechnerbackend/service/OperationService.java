package com.taschenrechner.taschenrechnerbackend.service;


import com.taschenrechner.taschenrechnerbackend.model.OperationModel;
import com.taschenrechner.taschenrechnerbackend.repository.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The operation sercice class.
 * @author Dany Chuissi
 */
@Service
public class OperationService {

    /**
     * The Operation repository.
     */
    @Autowired
    OperationRepository repository;

    /**
     * Get all operation from database.
     *
     * @return A list of {@link OperationModel}
     */
    public List<OperationModel> getAllOperations() {
        List<OperationModel> operationList = repository.findAll();

        if (operationList.size() > 0) {
            return operationList;
        } else {
            return new ArrayList<OperationModel>();
        }
    }

    /**
     * Create or update a custom value
     *
     * @param entity The Custom value to be created or updated
     * @return the created or updated {@link OperationModel}
     */
    public OperationModel createOperations(OperationModel entity) {

            entity = repository.save(entity);
            return entity;

    }
}
