package com.taschenrechner.taschenrechnerbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaschenrechnerBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaschenrechnerBackendApplication.class, args);
    }

}
