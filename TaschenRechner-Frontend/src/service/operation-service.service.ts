import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {OperationModel} from "../app/Model/OprationModel";

/**
 * the route to the operation controller in backend
 */
const API_URL = 'http://localhost:8080/api/operation';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})

export class OperationServiceService {

  constructor(private http: HttpClient) {}

  /**
   * get all operation
   */
  getAllOperation(): Observable<OperationModel[]> {
    return this.http.get<OperationModel[]>(API_URL, {responseType: 'json'});
  }

  /**
   * create a Operation
   */
  createOperation(operationText: string): Observable<OperationModel> {
    console.log(operationText)
    return this.http.post<OperationModel>(API_URL, {
      operationText
    }, httpOptions);
  }
}
