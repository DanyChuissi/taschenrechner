import { Component, OnInit } from '@angular/core';
import {OperationServiceService} from "../../../service/operation-service.service";
import {OperationModel} from "../../Model/OprationModel";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  operation: string ='';
  result: string = '';
  currentValues:string[] = [];
  lastValueIsNumber: boolean = false;
  error: boolean = false;
  serverError: boolean = false;
  savedOperation: OperationModel[]= [];
  lastValue: string = '';


  constructor(private operationService: OperationServiceService) { }

  ngOnInit(): void {
    this.getAllSavedOperation();
  }

  /**
   * handle on button click event
   * @param value
   */
  getPressButton(value: string): void{
    this.error = false;
    if(value === 'CE'){
      this.operation = '';
      this.result = '';
      this.currentValues = [];
    }
    else if(value === '='){
      this.calculate();
    }
    else if(value === '<_') {
      this.removeLastSymbol();
      this.setLastValueType(this.currentValues[this.currentValues.length-1]);
    }
    else{
      if( (!this.isNumeric(value) && this.currentValues[this.currentValues.length-1] !== ")" && !this.lastValueIsNumber)){
        this.removeLastSymbol();
      }
      if(this.result !== ''){
        this.currentValues = [];
        if(!this.isNumeric(value)){
          this.operation = this.result;
        }else{
          this.operation = '';
        }
        this.result = '';
      }
      this.currentValues.push(value);
      this.displayedOperation();
      this.setLastValueType(value);
    }
  }

  /**
   * set the oparation to be displayed
   */
  displayedOperation() : void{
    //console.log(this.lastValueIsNumber)
    if(this.currentValues.length > 0){
        this.operation += this.currentValues[this.currentValues.length - 1];
    }
  }

  /**
   * evaluate the operation string
   */
  calculate(): void {
    try{
      this.result = eval(this.operation);
      this.saveLastOperation(this.operation)
      this.error = false;
    }
    catch (e){
      console.log(e)
      this.error = true;
    }

  }

  /**
   * check if The input Value is a number
   * @param value the input value
   */
  isNumeric(value: string | number): boolean {
    return ((value != null) &&
      (value !== '') &&
      !isNaN(Number(value.toString())));
  }

  /**
   * remove last Input
   */
  removeLastSymbol() : void {
    this.currentValues.splice(-1,1)
    this.operation = '';
    this.currentValues.forEach(elem => {
      this.operation += elem;
    })
  }

  /**
   * Set the lastValueIsNumber attribute in base of input type
   * @param value
   */
  setLastValueType(value: string | number): void {
    if(!this.isNumeric(value) ){
      this.lastValueIsNumber = false;
    }else{
      this.lastValueIsNumber = true;
    }
  }

  /**
   * Save last oparation
   * @param operation the las oparation
   */
  saveLastOperation(operation: string): void{
    this.operationService.createOperation(operation).subscribe(
      data => {
        this.serverError = false;
      },
      err => {
        this.serverError = true;
      }
    );
  }

  /**
   * Get All saved oparation from Backend
   */
  getAllSavedOperation() : void {
    this.operationService.getAllOperation().subscribe(
      data => {
         this.savedOperation = data;
        this.operation = this.savedOperation[this.savedOperation.length-1].operationText;
        this.serverError = false;
      },
      err => {
        this.serverError = true;
      }
    );
  }
}
