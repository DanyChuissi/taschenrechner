export interface OperationModel {
  id?: string;
  operationText: string;
  createdAt?: string;
}
